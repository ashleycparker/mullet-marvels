//
//  Utilities.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 22/7/21.
//

import Foundation

enum AuthenticationState {
    case signedIn, signedOut
}

enum Provider: String {
    case facebook = "facebook.com", google = "google.com", apple = "apple.com", password = "password", none
}
