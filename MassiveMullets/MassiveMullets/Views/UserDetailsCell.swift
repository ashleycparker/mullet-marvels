//
//  UserDetailsCell.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 5/8/21.
//

import UIKit

class UserDetailsCell: UITableViewCell {

    @IBOutlet weak var displayImageView: UIImageView!
    @IBOutlet weak var displayNameLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func populate(_ user: AuthUser) {
        let displayname = user.displayname ?? ""
        let displayImage = user.displayimage ?? "default"
        let displayImageLink = user.displayimagelink ?? ""
        
        self.displayImageView.layer.cornerRadius = 25
        self.displayImageView.clipsToBounds = true
        
        displayNameLabel.text = displayname
    
        if displayImage == "default" {
            self.displayImageView.image = #imageLiteral(resourceName: "mulletmarvels")
        } else {
            DispatchQueue.global(qos: .background).async {
                if let url = URL(string: displayImageLink) {
                    if let data = try? Data(contentsOf: url) {
                        DispatchQueue.main.async {
                            let image = UIImage(data: data)
                            self.displayImageView.image = image
                        }
                    }
                }
            }
        }
    }
}
