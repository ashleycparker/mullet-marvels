//
//  PostDetailsCell.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 30/7/21.
//

import UIKit

class PostDetailsCell: UITableViewCell {

    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var postDateLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func populate(_ post: UserPost) {
        let postlink = post.postimagelink ?? ""
        let postDate = post.postdate ?? ""
        
        let localFormatter = ISO8601DateFormatter()
        localFormatter.timeZone = TimeZone(identifier: "UTC")
        guard let convertedPostDate = localFormatter.date(from: postDate) else { return }
        
        let formatedFormatter = DateFormatter()
        formatedFormatter.timeZone = TimeZone.current
        formatedFormatter.dateFormat = "d MMM, yyyy HH:mm"
        
        postDateLabel.text = formatedFormatter.string(from: convertedPostDate)
//        guard let data = post.imageData else { return }
        DispatchQueue.global(qos: .background).async {
            if post.imageData == nil {
                if let url = URL(string: postlink) {
                    if let data = try? Data(contentsOf: url) {
                        var filtered = DataManager.shared.postsArray.first { post in
                            post.uid == post.uid
                        }
                        filtered?.imageData = data
                        DispatchQueue.main.async {
                            let image = UIImage(data: data)
                            self.postImageView.image = image
                        }
                    }
                }
            } else {
                if let data = post.imageData {
                    DispatchQueue.main.async {
                        let image = UIImage(data: data)
                        self.postImageView.image = image
                    }
                }
            }
        }
    }
}
