//
//  DataManager.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 22/7/21.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift

class DataManager: NSObject {
    
    static let shared = DataManager()
    
    override init() {
        super.init()
        
        let settings = FirestoreSettings()
        Firestore.firestore().settings = settings
        db = Firestore.firestore()
    }

    var db: Firestore?
    
    var authState: AuthenticationState = .signedOut
    var authProvider: Provider = .none
    
    var user: AuthUser?
    var profileImage: UIImage?
    var profileImageSelected = false
    
    let onlineRef = Database.database().reference(withPath: "online")
    let usersRef = Database.database().reference(withPath: "users")
    let postsRef = Database.database().reference(withPath: "posts")

//    gs://massivemullets.appspot.com/
    let profileRef = Storage.storage().reference(withPath: "profile")
    let profilePostsRef = Storage.storage().reference(withPath: "profilePosts")
    
    var postsArray: Array<UserPost> = Array()

    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths.first!
    }
}
