//
//  ViewController.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 20/7/21.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKLoginKit
import FirebaseFirestore

import AuthenticationServices
import CryptoKit

class LoginVC: BaseVC, GIDSignInDelegate {
    
    var currentNonce: String?
    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var loginButtonView: UIView!
    @IBOutlet weak var loginGoogleButtonView: GIDSignInButton!
    @IBOutlet weak var loginFacebookButtonView: UIView!
    @IBOutlet weak var loginAppleButtonView: UIView!
    
//    var authStateDidChangeHandle: AuthStateDidChangeListenerHandle?
//    var auth: Auth?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        self.auth = Auth.auth()

        let gestureL = UITapGestureRecognizer(target: self, action: #selector(self.loginButtonPressed(_:)))
        let gestureG = UITapGestureRecognizer(target: self, action: #selector(self.loginGoogleButtonPressed(_:)))
        let gestureF = UITapGestureRecognizer(target: self, action: #selector(self.loginFacebookButtonPressed(_:)))
        let gestureA = UITapGestureRecognizer(target: self, action: #selector(self.loginAppleButtonPressed(_:)))
        
        self.loginButtonView.addGestureRecognizer(gestureL)
        self.loginGoogleButtonView.addGestureRecognizer(gestureG)
        self.loginFacebookButtonView.addGestureRecognizer(gestureF)
        self.loginAppleButtonView.addGestureRecognizer(gestureA)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.authStateDidChangeHandle =
          self.auth?.addStateDidChangeListener(self.updateUI(auth:user:))
        
        NotificationCenter.default.addObserver(self, selector: #selector(appleIDStateDidRevoked(_:)), name: ASAuthorizationAppleIDProvider.credentialRevokedNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let handle = self.authStateDidChangeHandle {
            self.auth?.removeStateDidChangeListener(handle)
        }
        
        NotificationCenter.default.removeObserver(self, name: ASAuthorizationAppleIDProvider.credentialRevokedNotification, object: nil)
    }

    @objc func loginButtonPressed(_ sender: UITapGestureRecognizer) {
        guard let email = emailText.text else { return }
        guard let password = passwordText.text else { return }
        
        self.auth?.signIn(withEmail: email, password: password) { authResult, error in
            if let error = error {
                print(error.localizedDescription)
            } else {
                DataManager.shared.authState = .signedIn
                self.retrieveUser(authResult?.user.uid ?? "", authResult?.user)
//                self.performSegue(withIdentifier: "FromLoginVC", sender: self)
            }
        }
    }
    
    @objc func loginGoogleButtonPressed(_ sender: UITapGestureRecognizer) {
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        
        if GIDSignIn.sharedInstance().currentUser == nil {
            GIDSignIn.sharedInstance().presentingViewController = UIApplication.shared.windows.first?.rootViewController
            GIDSignIn.sharedInstance().signIn()
        }
    }
    
    @objc func loginFacebookButtonPressed(_ sender: UITapGestureRecognizer) {
        LoginManager.init().logIn(permissions: [Permission.publicProfile, Permission.email], viewController: self) { (loginResult) in
            switch loginResult {
                case .success(let granted, let declined, let token):
                    if let facebookToken = token?.tokenString {
                        let credential = FacebookAuthProvider.credential(withAccessToken: facebookToken)
                        
                        self.firebaseAuthentication(withUser: credential)
                    }
            
                    print("granted: \(granted), declined: \(declined), token: \(token)")
                case .cancelled:
                    print("Login: cancelled.")
                case .failed(let error):
                    print("Login with error: \(error.localizedDescription)")
            }
        }
    }
    
    @objc func loginAppleButtonPressed(_ sender: UITapGestureRecognizer) {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]

        // Generate nonce for validation after authentication successful
        self.currentNonce = randomNonceString()
        // Set the SHA256 hashed nonce to ASAuthorizationAppleIDRequest
        request.nonce = sha256(currentNonce!)

        // Present Apple authorization form
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    @objc func appleIDStateDidRevoked(_ notification: Notification) {
        // Make sure user signed in with Apple
        if let user = self.auth?.currentUser {
            if
                let providerId = user.providerData.first?.providerID,
                providerId == "apple.com" {
                signOut()
            }
        }
    }
    
    func updateUI(auth: Auth, user: User?) {
        if let user = self.auth?.currentUser {
            DataManager.shared.authProvider = Provider(rawValue: user.providerData.first?.providerID ?? "none") ?? .none

            user.getIDTokenForcingRefresh(true) { idToken, error in
                if let error = error {
                    // Handle error
                    DataManager.shared.authState = .signedOut
                    return;
                }

                DataManager.shared.authState = .signedIn
            }
        } else {
            DataManager.shared.authState = .signedOut
        }
    }
    
    // MARK: - GIDSignInDelegate
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
            if let authentication = user.authentication {
                let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
                
                firebaseAuthentication(withUser: credential)
            }
        } else {
            print(error.debugDescription)
        }
    }
    
    private func firebaseAuthentication(withUser credential: AuthCredential) {
        Auth.auth().signIn(with: credential) { (results, error) in
            if let error = error {
                print(error.localizedDescription)
                DataManager.shared.user = nil
            } else {
                if let uid = results?.user.uid {
                    self.retrieveUser(uid, results?.user)
                }
                
//                DataManager.shared.authState = .signedIn
//                self.performSegue(withIdentifier: "FromLoginVC", sender: self)
            }
        }
    }
    
    private func retrieveUser(_ uid: String, _ user: User?) {
        let userRef = DataManager.shared.db?.collection("users")
        userRef?
            .whereField("uid", isEqualTo: uid)
//                .order(by: "ticks")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if querySnapshot?.documents.isEmpty ?? true {
                        if let user = user {
                            DataManager.shared.user = AuthUser(authData: user)
                            var userRef: DocumentReference? = nil
                            if let usr = DataManager.shared.user {
                                userRef = DataManager.shared.db?.collection("users").addDocument(data: usr.toDictionary()) { err in
                                    if let err = err {
                                        print("Error adding document: \(err)")
                                    } else {
                                        print("Document added with ID: \(userRef?.documentID)")
                                    }
                                }
                            }
    //                        let newUser = DataManager.shared.usersRef.childByAutoId()
    //                        newUser.setValue(DataManager.shared.user?.toAnyObject())
                            DataManager.shared.user?.ref = userRef

                            self.performSegue(withIdentifier: "LoginProfileVC", sender: self)
                        }
                    } else {
                        for document in querySnapshot!.documents {
                            DataManager.shared.user = AuthUser(snapshot: document.data())
                            DataManager.shared.user?.ref = document.reference
                            DataManager.shared.authState = .signedIn
                            
                            if DataManager.shared.user?.displayname == nil {
                                self.performSegue(withIdentifier: "LoginProfileVC", sender: self)
                            } else {
                                if let display = DataManager.shared.user?.displayimage, display != "default" {
                                    let path = DataManager.shared.getDocumentsDirectory().appendingPathComponent(display)
                                    
                                    if let data = try? Data(contentsOf: path) {
                                        DataManager.shared.profileImage = UIImage(data: data)
                                    }
                                } else {
                                    DataManager.shared.profileImage = #imageLiteral(resourceName: "mulletmarvels")
                                }
                                
                                self.performSegue(withIdentifier: "FromLoginVC", sender: self)
    //                            print(snapshot)
                            }
                            print("\(document.documentID) => \(document.data())")
                        }
                    }
                }
            }
    }
}

extension LoginVC {
    private func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: Array<Character> =
            Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length
        
        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                }
                return random
            }
            
            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }
                
                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        
        return result
    }

    private func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            return String(format: "%02x", $0)
        }.joined()
        
        return hashString
    }
}

extension LoginVC: ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}

extension LoginVC: ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {

            UserDefaults.standard.set(appleIDCredential.user, forKey: "appleAuthorizedUserIdKey")

            guard let nonce = currentNonce else {
                fatalError("Invalid state: A login callback was received, but no login request was sent.")
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
                print("Unable to fetch identity token")
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
            
          // Initialize a Firebase credential.
            let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                    idToken: idTokenString,
                                                    rawNonce: nonce)
          // Sign in with Firebase.
            firebaseAuthentication(withUser: credential)
    //      Auth.auth().signIn(with: credential) { (authResult, error) in
    //        if error {
    //          // Error. If error.code == .MissingOrInvalidNonce, make sure
    //          // you're sending the SHA256-hashed nonce as a hex string with
    //          // your request to Apple.
    //          print(error.localizedDescription)
    //          return
    //        }
            // User is signed in to Firebase with Apple.
            // ...
    //      }
        }
    }

    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        print("Sign in with Apple errored: \(error)")
    }
}

