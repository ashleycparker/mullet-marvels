//
//  ProfileImageVC.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 27/7/21.
//

import UIKit
import CropPickerView

class ProfileImageVC: BaseVC {
    
    @IBOutlet weak var profilePickerView: CropPickerView!
    @IBOutlet weak var cancelButtonView: UIView!
    @IBOutlet weak var acceptButtonView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.profilePickerView.delegate = self
        
        let gestureC = UITapGestureRecognizer(target: self, action: #selector(self.cancelViewPressed(_:)))
        let gestureA = UITapGestureRecognizer(target: self, action: #selector(self.acceptViewPressed(_:)))
        
        self.cancelButtonView.addGestureRecognizer(gestureC)
        self.acceptButtonView.addGestureRecognizer(gestureA)
        
        if let image = DataManager.shared.profileImage {
            profilePickerView.image = image
            profilePickerView.isSquare = true
        }
    }
    
    @objc func acceptViewPressed(_ sender: UITapGestureRecognizer) {
        self.profilePickerView.crop { (crop) in
            DataManager.shared.profileImage = crop.image?.resizeImage(with: CGSize(width: 500, height: 500))
            
            DataManager.shared.profileImageSelected = true
            
            self.back()
        }
    }
    
    @objc func cancelViewPressed(_ sender: UITapGestureRecognizer) {
        DataManager.shared.profileImage = nil
        
        self.back()
    }
}

// MARK: CropPickerViewDelegate
extension ProfileImageVC: CropPickerViewDelegate {
    func cropPickerView(_ cropPickerView: CropPickerView, result: CropResult) {

    }

    func cropPickerView(_ cropPickerView: CropPickerView, didChange frame: CGRect) {
        print("frame: \(frame)")
    }
}
