//
//  SplashVC.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 22/7/21.
//

import UIKit
import Firebase

import AuthenticationServices

class SplashVC: BaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let userID = UserDefaults.standard.string(forKey: "appleAuthorizedUserIdKey") {
            // Check Apple ID credential state
            ASAuthorizationAppleIDProvider().getCredentialState(forUserID: userID, completion: { [unowned self]
                credentialState, error in
                
                switch(credentialState) {
                case .authorized:
                    break
                case .notFound,
                     .transferred,
                     .revoked:
                    // Perform sign out
                    self.signOut()
                    break
                @unknown default:
                    break
                }
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.authStateDidChangeHandle =
          self.auth?.addStateDidChangeListener(self.updateUI(auth:user:))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let handle = self.authStateDidChangeHandle {
            self.auth?.removeStateDidChangeListener(handle)
        }
    }

    var processingUser = false
    
    func updateUI(auth: Auth, user: User?) {
        if let user = self.auth?.currentUser {
            processingUser = true
            DataManager.shared.authProvider = Provider(rawValue: user.providerData.first?.providerID ?? "none") ?? .none

            user.getIDTokenForcingRefresh(true) { idToken, error in
                if let _ = error {
                    self.processingUser = false
                    self.performSegue(withIdentifier: "LoginVC", sender: self)
                    return;
                }

                let userRef = DataManager.shared.db?.collection("users")
                userRef?
                    .whereField("uid", isEqualTo: user.uid)
    //                .order(by: "ticks")
                    .getDocuments() { (querySnapshot, err) in
                        if let err = err {
                            print("Error getting documents: \(err)")
                        } else {
                            if querySnapshot?.documents.isEmpty ?? true {
                                self.processingUser = false
                                self.performSegue(withIdentifier: "LoginVC", sender: self)
                            } else {
                                for document in querySnapshot!.documents {
                                    DataManager.shared.user = AuthUser(snapshot: document.data())
                                    DataManager.shared.user?.ref = document.reference
                                    DataManager.shared.authState = .signedIn
                                    
                                    if let display = DataManager.shared.user?.displayimage, display != "default" {
                                        let path = DataManager.shared.getDocumentsDirectory().appendingPathComponent(display)
                                        
                                        if let data = try? Data(contentsOf: path) {
                                            DataManager.shared.profileImage = UIImage(data: data)
                                        }
                                    } else {
                                        DataManager.shared.profileImage = #imageLiteral(resourceName: "mulletmarvels")
                                    }
                                    
                                    self.processingUser = false
                                    if DataManager.shared.user?.displayname == nil {
                                        self.performSegue(withIdentifier: "SplashProfileVC", sender: self)
                                    } else {
                                        DataManager.shared.postsArray = Array()
                                        
                                        self.performSegue(withIdentifier: "MainVC", sender: self)
                                    }
                                    print("\(document.documentID) => \(document.data())")
                                }
                            }
                        }
                    }
                
//                DataManager.shared.usersRef
//                    .queryOrdered(byChild: "uid")
//                    .queryEqual(toValue: user.uid)
//                    .observeSingleEvent(of: .value, with: { (snapshot) in
//
//                        for child in snapshot.children {
//                            if let snapshot = child as? DataSnapshot {
//                                DataManager.shared.user = AuthUser(snapshot: snapshot)
//                                DataManager.shared.authState = .signedIn
//
//                                if let display = DataManager.shared.user?.displayimage, display != "default" {
//                                    let path = DataManager.shared.getDocumentsDirectory().appendingPathComponent(display)
//
//                                    if let data = try? Data(contentsOf: path) {
//                                        DataManager.shared.profileImage = UIImage(data: data)
//                                    }
//                                } else {
//                                    DataManager.shared.profileImage = #imageLiteral(resourceName: "mulletmarvels")
//                                }
//
//                                self.processingUser = false
//                                if DataManager.shared.user?.displayname == nil {
//                                    self.performSegue(withIdentifier: "SplashProfileVC", sender: self)
//                                } else {
//                                    DataManager.shared.postsArray = Array()
//
//                                    self.performSegue(withIdentifier: "MainVC", sender: self)
//                                }
//                            }
//                        }
//                })
            }
        } else {
            if !processingUser {
                self.performSegue(withIdentifier: "LoginVC", sender: self)
            }
        }
    }
}
