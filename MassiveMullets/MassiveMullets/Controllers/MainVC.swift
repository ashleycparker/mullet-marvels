//
//  MainVC.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 22/7/21.
//

import UIKit
import Firebase
import MaterialActivityIndicator

class MainVC: BaseVC {

    @IBOutlet weak var postsTableView: UITableView!
    
    @IBOutlet weak var loadingView: UIView!
    
    @IBOutlet weak var loadingProgressView: MaterialActivityIndicatorView!
    
    @IBOutlet weak var addButtonView: UIView!
    @IBOutlet weak var addImageView: UIImageView!
    
//    @IBOutlet weak var displayImageView: UIImageView!
    @IBOutlet weak var displayNameLabel: UILabel!
    
    var newPost: UserPost?
    
    let cellIdentifier = "PostCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.displayImageView.layer.cornerRadius = 35
        
        self.postsTableView.delegate = self
        self.postsTableView.dataSource = self
        self.postsTableView.isHidden = true
        
        let rowHeight = self.postsTableView.frame.width
        
        self.postsTableView.rowHeight = rowHeight + 40 // 300
        self.postsTableView.separatorStyle = .none
        
        if let user = self.auth?.currentUser {
            let postsRef = DataManager.shared.db?.collection("posts")
            postsRef?
                .whereField("uid", isEqualTo: user.uid)
                .order(by: "ticks")
                .getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        for document in querySnapshot!.documents {
                            var posts = UserPost(snapshot: document.data())
                            posts?.ref = document.reference
                            let path = DataManager.shared.getDocumentsDirectory().appendingPathComponent(posts?.postimage ?? "")

                            if let data = try? Data(contentsOf: path) {
                                posts?.imageData = data
                            }

                            if let posts = posts {
                                DataManager.shared.postsArray.insert(posts, at: 0)
                            }
                            print("\(document.documentID) => \(document.data())")
                        }
                    }
                    
                    self.postsTableView.isHidden = false
                    self.postsTableView.reloadData()
            }
        }
        
        self.authStateDidChangeHandle = self.auth?.addStateDidChangeListener(self.updateUI(auth:user:))
        
        let gestureA = UITapGestureRecognizer(target: self, action: #selector(self.addButtonPressed(_:)))
        self.addButtonView.addGestureRecognizer(gestureA)
        
        self.addImageView.image = #imageLiteral(resourceName: "add").tint(with: .white)
        
        self.loadingView.isHidden = false
        self.loadingProgressView.startAnimating()
        self.view.bringSubviewToFront(self.loadingView)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let handle = self.authStateDidChangeHandle {
            self.auth?.removeStateDidChangeListener(handle)
        }
    }
    
    func updateUI(auth: Auth, user: User?) {
        if let user = self.auth?.currentUser {
            let userRef = DataManager.shared.db?.collection("users")
            userRef?
                .whereField("uid", isEqualTo: user.uid)
//                .order(by: "ticks")
                .getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        for document in querySnapshot!.documents {
                            DataManager.shared.user = AuthUser(snapshot: document.data())
                            DataManager.shared.user?.ref = document.reference
                            
                            if let displayname = DataManager.shared.user?.displayname {
                                self.displayNameLabel.text = displayname
                            }
                            
                            print("\(document.documentID) => \(document.data())")
                        }
                        
                        self.loadingView.isHidden = true
                        self.loadingProgressView.stopAnimating()
                        self.view.sendSubviewToBack(self.loadingView)
                    }
                }
//                DataManager.shared.usersRef
//                    .queryOrdered(byChild: "uid")
//                    .queryEqual(toValue: user.uid)
//                    .observeSingleEvent(of: .value, with: { (snapshot) in
//
//                        if snapshot.exists() {
//                            for child in snapshot.children {
//                                if let snapshot = child as? DataSnapshot {
//                                    DataManager.shared.user = AuthUser(snapshot: snapshot)
////                                    if let imagename = DataManager.shared.user?.displayimage, imagename == "default" {
////                                        self.displayImageView.image = #imageLiteral(resourceName: "mulletmarvels")
////                                    } else {
////                                        self.displayImageView.image = DataManager.shared.profileImage
////                                    }
//
//                                    if let displayname = DataManager.shared.user?.displayname {
//                                        self.displayNameLabel.text = displayname
//                                    }
//                                }
//                            }
//                        }
//
//                        self.loadingView.isHidden = true
//                        self.loadingProgressView.stopAnimating()
//                        self.view.sendSubviewToBack(self.loadingView)
//                })
                
            let onlineRef = DataManager.shared.onlineRef.child(user.uid)
            onlineRef.setValue(user.email)
            onlineRef.onDisconnectRemoveValue()
        }
    }
    
    @objc func addButtonPressed(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "CameraVC", sender: self)
    }
    
    @IBAction func unwind( _ seg: UIStoryboardSegue) {
//        self.tabBarController?.tabBar.isHidden = false
//        self.tabBarController?.tabBar.layer.zPosition = -0
        if let newPost = self.newPost {
            DataManager.shared.postsArray.insert(newPost, at: 0)
            self.newPost = nil
        }
        
        self.setTabBarVisible(visible: !self.tabBarIsVisible(), animated: true)
        
        self.postsTableView.beginUpdates()
        let section = self.postsTableView.numberOfSections - 1
        let indexPath = IndexPath(item: 0, section: section)
        self.postsTableView.insertRows(at: [indexPath], with: .top)
        self.postsTableView.endUpdates()
//        self.postsTableView.reloadData()
    }
}

extension MainVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.shared.postsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = DataManager.shared.postsArray[indexPath.row]
        
        if let cell = self.postsTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PostDetailsCell {
            cell.populate(model)
            
            return cell
        }
        
        return UITableViewCell()
    }
}
