//
//  CameraVC.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 29/7/21.
//

import UIKit
import AVFoundation

class CameraVC: BaseVC {

    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var captureView: UIView!
    
    @IBOutlet weak var captureButtonView: UIView!
    @IBOutlet weak var captureImageView: UIImageView!
    
    @IBOutlet weak var rotateButtonView: UIView!
    @IBOutlet weak var rotateImageView: UIImageView!
    
    @IBOutlet weak var closeButtonView: UIView!
    @IBOutlet weak var closeImageView: UIImageView!
    
    var captureSession: AVCaptureSession!
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var activeCamera: AVCaptureDevice?
    
    var captureImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.captureImageView.image = #imageLiteral(resourceName: "cameracapture").tint(with: .white)
        self.rotateImageView.image = #imageLiteral(resourceName: "rotate").tint(with: .white)
        self.closeImageView.image = #imageLiteral(resourceName: "close").tint(with: .white)
        
        let gestureC = UITapGestureRecognizer(target: self, action: #selector(self.captureButtonPressed(_:)))
        let gestureR = UITapGestureRecognizer(target: self, action: #selector(self.rotateButtonPressed(_:)))
        let gestureCl = UITapGestureRecognizer(target: self, action: #selector(self.closeButtonPressed(_:)))
        self.captureButtonView.addGestureRecognizer(gestureC)
        self.rotateButtonView.addGestureRecognizer(gestureR)
        self.closeButtonView.addGestureRecognizer(gestureCl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        self.tabBarController?.tabBar.isHidden = true
//        self.tabBarController?.tabBar.layer.zPosition = -1
        self.setTabBarVisible(visible: !self.tabBarIsVisible(), animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .high
        
        //need to change to check for ultra wide device
        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
            else {
                print("Unable to access back camera!")
                return
        }
        
        activeCamera = backCamera
        
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            
            stillImageOutput = AVCapturePhotoOutput()

            if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                captureSession.addInput(input)
                captureSession.addOutput(stillImageOutput)
                setupLivePreview()
            }
            
        }
        catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
        
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(handlePinch(_:)))
        //should be previewView but captureView is in front
        captureView.addGestureRecognizer(pinch)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.captureSession.stopRunning()
    }

    func setupLivePreview() {
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.connection?.videoOrientation = .portrait
        previewView.layer.addSublayer(videoPreviewLayer)
        
        DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
            self.captureSession.startRunning()
            
            DispatchQueue.main.async {
                self.videoPreviewLayer.frame = self.previewView.bounds
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PreviewVC" {
            if let vc = segue.destination as? PreviewVC {
                vc.captureImage = self.captureImage
            }
        }
    }
    
    var zoomScaleRange: ClosedRange<CGFloat> = 0.5...5
    var initialScale: CGFloat = 0.0
    
    @IBAction func handlePinch(_ sender: UIPinchGestureRecognizer) {

        guard let device = activeCamera else { return }
        
        switch sender.state {
            case .began:
                initialScale = device.videoZoomFactor
            case .changed:
                let minAvailableZoomScale = device.minAvailableVideoZoomFactor
                let maxAvailableZoomScale = device.maxAvailableVideoZoomFactor
                let availableZoomScaleRange = minAvailableZoomScale...maxAvailableZoomScale
                let resolvedZoomScaleRange = zoomScaleRange.clamped(to: availableZoomScaleRange)

                let resolvedScale = max(resolvedZoomScaleRange.lowerBound, min(sender.scale * initialScale, resolvedZoomScaleRange.upperBound))

                do {
                    try device.lockForConfiguration()
                } catch {
                    return
                }

                device.videoZoomFactor = resolvedScale
                device.unlockForConfiguration()
                
            default:
                return
            }
    }
    
    @objc func closeButtonPressed(_ sender: UITapGestureRecognizer) {
//        self.tabBarController?.tabBar.isHidden = false
//        self.tabBarController?.tabBar.layer.zPosition = -0
        self.setTabBarVisible(visible: !self.tabBarIsVisible(), animated: true)
        
        self.back()
    }
    
    @objc func rotateButtonPressed(_ sender: UITapGestureRecognizer) {
        //Change camera source
        if let session = captureSession {
            //Remove existing input
            guard let currentCameraInput: AVCaptureInput = session.inputs.first else {
                return
            }
            
            //Indicate that some changes will be made to the session
            session.beginConfiguration()
            session.removeInput(currentCameraInput)

            //Get new input
            var newCamera: AVCaptureDevice! = nil
            if let input = currentCameraInput as? AVCaptureDeviceInput {
                if (input.device.position == .back) {
                    newCamera = cameraWithPosition(position: .front)
                } else {
                    newCamera = cameraWithPosition(position: .back)
                }
            }
            
            activeCamera = newCamera
            
            //Add input to session
            var err: NSError?
            var newVideoInput: AVCaptureDeviceInput!
            do {
                newVideoInput = try AVCaptureDeviceInput(device: newCamera)
            } catch let err1 as NSError {
                err = err1
                newVideoInput = nil
            }

            if newVideoInput == nil || err != nil {
                print("Error creating capture device input: \(err?.localizedDescription)")
            } else {
                session.addInput(newVideoInput)
            }

            //Commit all the configuration changes at once
            session.commitConfiguration()
        }
    }

    // Find a camera with the specified AVCaptureDevicePosition, returning nil if one is not found
    func cameraWithPosition(position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let discoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .unspecified)
        for device in discoverySession.devices {
            if device.position == position {
                return device
            }
        }

        return nil
    }
    
    @objc func captureButtonPressed(_ sender: UITapGestureRecognizer) {
        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        settings.isHighResolutionPhotoEnabled = false
        stillImageOutput.capturePhoto(with: settings, delegate: self)
    }
    
    func fixOrientation(image: UIImage) -> UIImage? {
        if image.imageOrientation == .up {
            return image
        }

        // We need to calculate the proper transformation to make the image upright.
        // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
        var transform = CGAffineTransform.identity

        switch image.imageOrientation {
           case .down, .downMirrored:
            transform = transform.translatedBy(x: image.size.width, y: image.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: image.size.width, y: 0)
            transform = transform.rotated(by:  CGFloat(Double.pi / 2))
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: image.size.height)
            transform = transform.rotated(by:  -CGFloat(Double.pi / 2))
        default:
            break
        }

        switch image.imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: image.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: image.size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        default:
            break
        }

        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        guard let context = CGContext(data: nil, width: Int(image.size.width), height: Int(image.size.height), bitsPerComponent: image.cgImage!.bitsPerComponent, bytesPerRow: 0, space: image.cgImage!.colorSpace!, bitmapInfo: image.cgImage!.bitmapInfo.rawValue) else {
            return nil
        }

        context.concatenate(transform)

        switch image.imageOrientation {
          case .left, .leftMirrored, .right, .rightMirrored:
            context.draw(image.cgImage!, in: CGRect(x: 0, y: 0, width: image.size.height, height: image.size.width))
           default:
              context.draw(image.cgImage!, in: CGRect(origin: .zero, size: image.size))
        }

        // And now we just create a new UIImage from the drawing context
        guard let CGImage = context.makeImage() else {
            return nil
        }

        return UIImage(cgImage: CGImage)
    }
}

extension CameraVC: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let imageData = photo.fileDataRepresentation() else { return }
        
//        let cropRect = CGRect(x: captureView.frame.origin.x - previewView.frame.origin.x,
//                              y: captureView.frame.origin.y - previewView.frame.origin.y,
//                              width: captureView.frame.width,
//                              height: captureView.frame.height)
        
        if let image = UIImage(data: imageData) {
//        captureImageView.image = image
            if let fixed = self.fixOrientation(image: image) {
                let fixedHeight = fixed.size.height
                let fixedSize = fixed.size.width
                
                let startY = (fixedHeight - fixedSize) / 2
//                let imageViewScale = max(fixed.size.width / previewView.frame.width,
//                                         fixed.size.height / previewView.frame.height)
                
//                let fixedSize = min(fixed.size.width, cropRect.size.width * imageViewScale)
                
                // Scale cropRect to handle images larger than shown-on-screen size
                let cropZone = CGRect(x: 0,
                                      y: startY,
                                      width: fixedSize,
                                      height: fixedSize)
                
                // Perform cropping in Core Graphics
                guard let cutImageRef: CGImage = fixed.cgImage?.cropping(to: cropZone) else { return }
                
                // Return image to UIImage
                let croppedImage: UIImage = UIImage(cgImage: cutImageRef)
                //return croppedImage
                
                self.captureImage = croppedImage
                
                self.performSegue(withIdentifier: "PreviewVC", sender: self)
                print("fixed")
            }
        }
    }
}
