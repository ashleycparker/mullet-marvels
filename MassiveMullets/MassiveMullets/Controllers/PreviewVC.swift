//
//  PreviewVC.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 29/7/21.
//

import UIKit
import FirebaseCore
import FirebaseFirestore
import FirebaseFirestoreSwift

class PreviewVC: BaseVC {

    var captureImage: UIImage?
    var post: UserPost?
    
    @IBOutlet weak var previewImageView: UIImageView!
    
    @IBOutlet weak var doneButtonView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gestureD = UITapGestureRecognizer(target: self, action: #selector(self.doneButtonPressed(_:)))
        self.doneButtonView.addGestureRecognizer(gestureD)
        
        self.post = nil
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let captureImage = captureImage {
            self.previewImageView.image = captureImage
        }
    }
    
    @objc func doneButtonPressed(_ sender: UITapGestureRecognizer) {
        
        guard let user = DataManager.shared.user else { return }
        
        let date = Date()
        // Create Date Formatter
        let localFormatter = ISO8601DateFormatter()
        localFormatter.timeZone = TimeZone(identifier: "UTC")
        
        // Convert Date to String
        let postdate = localFormatter.string(from: date)
        let ticks = date.timeIntervalSince1970

        let uid = user.uid //UUID().uuidString
        var posts = UserPost(uid: uid, postdate: postdate, comment: "test", ticks: ticks)

        var postRef: DocumentReference? = nil
        postRef = DataManager.shared.db?.collection("posts").addDocument(data: posts.toDictionary()) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(postRef?.documentID)")
            }
        }
        
        let key = postRef?.documentID
        let filename = "posts_\(user.uid)_\(key ?? "default").jpeg"
        posts.postimage = filename
        posts.ref = postRef
        
        let destination = DataManager.shared.getDocumentsDirectory().appendingPathComponent(filename)
        if let imageData = self.captureImage?.jpegData(compressionQuality: 0.4) {
            posts.imageData = imageData
            do {
                try imageData.write(to: destination)
            } catch {
                print("Something went wrong!")
            }
        }
        
//        DataManager.shared.postsArray.insert(posts, at: 0)
        
        DispatchQueue.global(qos: .background).async {
            let profile = DataManager.shared.profilePostsRef.child(user.uid).child(filename)
            
            if let imageData = self.captureImage?.jpegData(compressionQuality: 0.4) {
                
                let _ = profile.putData(imageData, metadata: nil) { (metadata, error) in
//                        guard let metadata = metadata else {
//                            // Uh-oh, an error occurred!
//                            return
//                        }
                    // Metadata contains file metadata such as size, content-type.
//                        let size = metadata.size
                    // You can also access to download URL after upload.
                    profile.downloadURL { (url, error) in
                        guard let downloadURL = url else {
                            // Uh-oh, an error occurred!
                            return
                        }
                        
                        postRef?.updateData([
                            "postimagelink" : downloadURL.absoluteString,
                            "postimage" : filename
                        ]) { err in
                            if let err = err {
                                print("Error updating document: \(err)")
                            } else {
                                print("Document successfully updated")
                            }
                        }
                        
                        var forUpdate = DataManager.shared.postsArray.first { posts in
                            posts.uid == uid
                        }
                        
                        forUpdate?.postimagelink = downloadURL.absoluteString
                        
                        print("post image submitted")
                    }
                }
            }
        }
        
        self.post = posts
        performSegue(withIdentifier: "unwindToMainVC", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "unwindToMainVC" {
            if let vc = segue.destination as? MainVC {
                vc.newPost = self.post
            }
        }
    }
}
