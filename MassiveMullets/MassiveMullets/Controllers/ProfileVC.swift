//
//  ProfileVC.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 26/7/21.
//

import UIKit

class ProfileVC: BaseVC, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var updateButtonView: UIView!
    @IBOutlet weak var displayImageView: UIImageView!
    @IBOutlet weak var displayNameLabel: UITextField!
    @IBOutlet weak var iconView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        self.displayNameLabel.delegate = self
        
        let gestureU = UITapGestureRecognizer(target: self, action: #selector(self.updateButtonPressed(_:)))
        let gestureE = UITapGestureRecognizer(target: self, action: #selector(self.iconViewPressed(_:)))
        
        self.updateButtonView.addGestureRecognizer(gestureU)
        self.iconView.addGestureRecognizer(gestureE)
        
        self.iconView.layer.cornerRadius = 100
        self.iconView.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let image = DataManager.shared.profileImage, DataManager.shared.profileImageSelected {
            displayImageView.image = image
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @objc func updateButtonPressed(_ sender: UITapGestureRecognizer) {
        guard let displayname = self.displayNameLabel.text else { return }
        var filename = "default"
        if let user = DataManager.shared.user, DataManager.shared.profileImageSelected {
            filename = "profile_pics_\(user.uid).jpeg"
            
            DispatchQueue.global(qos: .background).async {
                
                let profile = DataManager.shared.profileRef.child(filename)
                
                if let imageData = DataManager.shared.profileImage?.jpegData(compressionQuality: 0.4) {
                    let _ = profile.putData(imageData, metadata: nil) { (metadata, error) in
//                        guard let metadata = metadata else {
//                            // Uh-oh, an error occurred!
//                            return
//                        }
                        // Metadata contains file metadata such as size, content-type.
//                        let size = metadata.size
                        // You can also access to download URL after upload.
                        profile.downloadURL { (url, error) in
                            guard let downloadURL = url else {
                                // Uh-oh, an error occurred!
                                return
                            }
                            
                            if let user = DataManager.shared.user {
                                user.ref?.updateData([
                                    "displayimagelink" : downloadURL.absoluteString
                                ])
                            }
                        }
                    }
                }
                
                DataManager.shared.profileImageSelected = false
            }
        }
        
        if let user = DataManager.shared.user {
            if filename != "default" {
                let destination = DataManager.shared.getDocumentsDirectory().appendingPathComponent(filename)

                if let imageData = DataManager.shared.profileImage?.jpegData(compressionQuality: 0.4) {
                    do {
                        try imageData.write(to: destination)
                    } catch {
                        print("\(error.localizedDescription) Something went wrong!")
                    }
                }
            } else {
                DataManager.shared.profileImage = #imageLiteral(resourceName: "mulletmarvels")
            }
            
            DataManager.shared.user?.displayname = displayname
            
            user.ref?.updateData([
                "displayname" : displayname,
                "displayimage" : filename
            ])
        }
        
        self.performSegue(withIdentifier: "FromProfileVC", sender: self)
    }
    
    func camera()
    {
        let picker = UIImagePickerController()
        picker.delegate = self;
        picker.sourceType = UIImagePickerController.SourceType.camera

        self.present(picker, animated: true, completion: nil)
    }

    func photoLibrary()
    {
        let picker = UIImagePickerController()
        picker.delegate = self;
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary

        self.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
//            self.displayImageView.image = image
            DataManager.shared.profileImageSelected = false
            DataManager.shared.profileImage = image
            self.performSegue(withIdentifier: "ProfileImageVC", sender: self)
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @objc func iconViewPressed(_ sender: UITapGestureRecognizer) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))

        actionSheet.addAction(UIAlertAction(title: "Library", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))

        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.navigationController?.present(actionSheet, animated: true, completion: nil)
    }
}
