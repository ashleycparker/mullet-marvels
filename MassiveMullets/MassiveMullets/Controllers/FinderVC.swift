//
//  FinderVC.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 4/8/21.
//

import UIKit
import Firebase
import FirebaseFirestore

class FinderVC: BaseVC {
    
    var userRef: CollectionReference?
    var users: Array<AuthUser>?
    
    @IBOutlet weak var finderTableView: UITableView!
    
    let cellIdentifier = "UserCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userRef = DataManager.shared.db?.collection("users")
        
        self.finderTableView.delegate = self
        self.finderTableView.dataSource = self
        self.finderTableView.rowHeight = 60
        self.finderTableView.separatorStyle = .none
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let user = self.auth?.currentUser {
//            let userRef = DataManager.shared.db?.collection("users")
            self.users = Array()
            userRef?
                .whereField("uid", isNotEqualTo: user.uid)
    //                .order(by: "ticks")
                .getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
//                        if querySnapshot?.documents.isEmpty ?? true {
//
//                        } else {
                            for document in querySnapshot!.documents {
                                if let user = AuthUser(snapshot: document.data()) {
                                    self.users?.append(user)
                                    print("\(document.documentID) => \(document.data())")
                                }
                            }
//                        }
                        self.finderTableView.reloadData()
                    }
                }
        }
    }
}

extension FinderVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = self.users?[indexPath.row] else { return UITableViewCell() }
        
        if let cell = self.finderTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? UserDetailsCell {
            cell.populate(model)
            
            return cell
        }
        
        return UITableViewCell()
    }
}
