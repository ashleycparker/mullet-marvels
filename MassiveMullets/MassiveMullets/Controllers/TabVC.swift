//
//  TabVC.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 2/8/21.
//

import UIKit
import Firebase

class TabVC: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.selectedIndex = 1

        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        
        if let tabs = self.tabBar.items {
            // home tab
            if let home = #imageLiteral(resourceName: "home")
                .resizeImage(with: CGSize(width: 30, height: 30))
                .tint(with: .white) {
                tabs.first?.image = home.withRenderingMode(.alwaysOriginal)
                tabs.first?.selectedImage = home.withRenderingMode(.alwaysOriginal)
                tabs.first?.imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: -10, right: 0)
//                tabs.first?.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 10)
            }
            
            // profile tab
            if let image = DataManager.shared.profileImage?
                .resizeImage(with: CGSize(width: 50, height: 50))
                .roundedImage(with: 25)
                .withRenderingMode(.alwaysOriginal) {
//                tabs[1].title = DataManager.shared.user?.displayname
                tabs[1].image = image
                tabs[1].selectedImage = image
                tabs[1].imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: -10, right: 0)
//                tabs[1].imageInsets = UIEdgeInsets(top: -10, left: 0, bottom: 10, right: 0)
//                tabs[1].titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 10)
            }
            
            // settings tab
            if let settings = #imageLiteral(resourceName: "settings")
                .resizeImage(with: CGSize(width: 30, height: 30))
                .tint(with: .white) {
                tabs.last?.image = settings.withRenderingMode(.alwaysOriginal)
                tabs.last?.selectedImage = settings.withRenderingMode(.alwaysOriginal)
                tabs.last?.imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: -10, right: 0)
//                tabs.last?.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 10)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}
