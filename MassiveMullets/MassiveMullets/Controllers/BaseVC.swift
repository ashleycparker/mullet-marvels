//
//  BaseVC.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 22/7/21.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKLoginKit

class BaseVC: UIViewController {

    var authStateDidChangeHandle: AuthStateDidChangeListenerHandle?
    var auth: Auth?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.auth = Auth.auth()
    }
    
    func signOut() {
        if DataManager.shared.authProvider == .facebook {
            LoginManager.init().logOut()
        }
        
        if DataManager.shared.authProvider == .google {
            GIDSignIn.sharedInstance().signOut()
        }
    
        if DataManager.shared.authProvider == .apple {
            UserDefaults.standard.removeObject(forKey: "appleAuthorizedUserIdKey")
        }
        
        do {
            try Auth.auth().signOut()
            
            DataManager.shared.authState = .signedOut
        } catch let signOutError as NSError {
            print(signOutError.localizedDescription)
        }
        
        if let user = DataManager.shared.user {
            let onlineRef = DataManager.shared.onlineRef.child(user.uid)
            onlineRef.removeValue()
            
            DataManager.shared.user = nil
            DataManager.shared.profileImage = nil
            DataManager.shared.profileImageSelected = false
            DataManager.shared.postsArray = Array()
        }
        
        self.tabBarController?.navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    func back() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setTabBarVisible(visible: Bool, animated: Bool) {
        // hide tab bar
        let frame = self.tabBarController?.tabBar.frame
        let height = frame?.size.height
        let offsetY = (visible ? -height! : height)
//        print ("offsetY = \(offsetY)")

        // zero duration means no animation
        let duration: TimeInterval = (animated ? 0.3 : 0.0)

        // animate tabBar
        if frame != nil {
            UIView.animate(withDuration: duration) {
                self.tabBarController?.tabBar.frame = frame!.offsetBy(dx: 0, dy: offsetY!)
                self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height + offsetY!)
                self.view.setNeedsDisplay()
                self.view.layoutIfNeeded()
                
//                self.tabBarController?.tabBar.isHidden = visible ? false : true
//                self.tabBarController?.tabBar.layer.zPosition = visible ? -0 : -1
                
                return
            }
        }
    }



    func tabBarIsVisible() -> Bool {
        return self.tabBarController?.tabBar.frame.origin.y ?? 0 < UIScreen.main.bounds.height
    }
}
