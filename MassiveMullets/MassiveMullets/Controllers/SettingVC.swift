//
//  SettingVC.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 2/8/21.
//

import UIKit

class SettingVC: BaseVC {

    @IBOutlet weak var signoutButtonView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let gestureS = UITapGestureRecognizer(target: self, action: #selector(self.signoutButtonPressed(_:)))
        self.signoutButtonView.addGestureRecognizer(gestureS)
    }
    
    @objc func signoutButtonPressed(_ sender: UITapGestureRecognizer) {
        self.signOut()
    }
}
