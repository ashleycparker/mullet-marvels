//
//  UserPost.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 30/7/21.
//

import Foundation
import Firebase
import FirebaseFirestore

struct UserPost {
  
    var ref: DocumentReference?
    
    let uid: String
    let postdate: String?
    var postimage: String?
    var postimagelink: String?
    let comment: String?
    let ticks: Double?
    var imageData: Data?
    
    init(uid: String, postdate: String, comment: String, ticks: Double) {
        self.ref = nil
        self.uid = uid
        self.postdate = postdate
        self.postimage = nil
        self.postimagelink = nil
        self.comment = comment
        self.ticks = ticks
    }
    
    init?(snapshot: Dictionary<String, Any>) {
        guard
//            let value = snapshot.value as? [String: AnyObject],
            let uid = snapshot["uid"] as? String else {
            return nil
        }
      
        let postdate = snapshot["postdate"] as? String
        let postimage = snapshot["postimage"] as? String
        let postimagelink = snapshot["postimagelink"] as? String
        let comment = snapshot["comment"] as? String
        let ticks = snapshot["ticks"] as? Double
        
//        self.ref = snapshot.ref
//        self.key = snapshot.key
        self.uid = uid
        self.postdate = postdate
        self.postimage = postimage
        self.postimagelink = postimagelink
        self.comment = comment
        self.ticks = ticks
    }
    
    func toAnyObject() -> Any {
        return [
            "uid": uid,
            "postdate": postdate,
            "postimage": postimage,
            "postimagelink": postimagelink,
            "comment": comment,
            "ticks": ticks
        ]
    }
    
    func toDictionary() -> [String: Any] {
        return [
            "uid": uid,
            "postdate": postdate,
            "postimage": postimage,
            "postimagelink": postimagelink,
            "comment": comment,
            "ticks": ticks
        ]
    }
}
