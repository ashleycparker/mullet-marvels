//
//  AuthUser.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 23/7/21.
//

import Foundation
import Firebase
import FirebaseFirestore

struct AuthUser {
  
    var ref: DocumentReference?
//    let key: String
    
    let uid: String
    var displayname: String?
    let displayimage: String?
    let displayimagelink: String?
    let email: String

//    ref
//    groceryItem.ref?.updateChildValues([
//      "completed": toggledCompletion
//      ])
//
    init(authData: Firebase.User) {
        ref = nil
        uid = authData.uid
        email = authData.email!
        displayname = nil
        displayimage = nil
        displayimagelink = nil
    }
//  
//    init(uid: String, email: String) {
//        self.ref = nil
//        self.uid = uid
//        self.email = email
//    }
//    
    init?(snapshot: Dictionary<String, Any>) {
        guard
//            let value = snapshot.value as? [String: AnyObject],
            let uid = snapshot["uid"] as? String,
            let email = snapshot["email"] as? String else {
            return nil
        }
      
        let displayname = snapshot["displayname"] as? String
        let displayimage = snapshot["displayimage"] as? String
        let displayimagelink = snapshot["displayimagelink"] as? String
        
//        self.key = snapshot.key
        self.uid = uid
        self.email = email
        self.displayname = displayname
        self.displayimage = displayimage
        self.displayimagelink = displayimagelink
    }
    
    func toAnyObject() -> Any {
        return [
            "uid": uid,
            "email": email,
            "displayname": displayname,
            "displayimage": displayimage,
            "displayimagelink": displayimagelink
        ]
    }
    
    func toDictionary() -> [String: Any] {
        return [
            "uid": uid,
            "email": email,
            "displayname": displayname,
            "displayimage": displayimage,
            "displayimagelink": displayimagelink
        ]
    }
}
