//
//  UIImage.swift
//  MassiveMullets
//
//  Created by Ashley Parker on 27/7/21.
//

import UIKit

extension UIImage {
    func resizeImage(with size: CGSize) -> UIImage {
        let currentsize = self.size
        let widthRatio  = size.width  / currentsize.width
        let heightRatio = size.height / currentsize.height
        let newSize = widthRatio > heightRatio ?  CGSize(width: currentsize.width * heightRatio, height: currentsize.height * heightRatio) : CGSize(width: currentsize.width * widthRatio,  height: currentsize.height * widthRatio)
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
    
    func roundedImage(with size: CGFloat) -> UIImage {
        let rect = CGRect(origin:CGPoint(x: 0, y: 0), size: self.size)
        UIGraphicsBeginImageContextWithOptions(self.size, false, 1)
        UIBezierPath(
            roundedRect: rect,
            cornerRadius: size
            ).addClip()
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
    
    func tint(with fillColor: UIColor) -> UIImage? {
            let image = withRenderingMode(.alwaysTemplate)
            UIGraphicsBeginImageContextWithOptions(size, false, scale)
            fillColor.set()
            image.draw(in: CGRect(origin: .zero, size: size))

            guard let imageColored = UIGraphicsGetImageFromCurrentImageContext() else {
                return nil
            }
            
            UIGraphicsEndImageContext()
            return imageColored
        }
}
